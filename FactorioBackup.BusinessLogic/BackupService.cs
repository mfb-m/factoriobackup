﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FactorioBackup.BusinessLogic
{
    public class BackupService
    {
        private string _SourceFile;
        private string _DestinationPath;
        private ResultService ResultService;
        private Task BackupTask;

        public BackupService(string sourceFile, string destinationPath)
        {
            _SourceFile = sourceFile;
            _DestinationPath = destinationPath;
            ResultService = new ResultService();
        }

        public async Task<ResultService> DoBackupAsync()
        {
            await CheckDirectoryExistsAsync();
            var fileName = await GetFileNameAsync();
            var deltinationFile = await BuildFileNameAsync(fileName);
            await CopyFileAsync(_SourceFile, deltinationFile);
            return ResultService;
        }

        private Task CheckDirectoryExistsAsync()
        {
            return Task.Run(() =>
            {
                if (!Directory.Exists(_DestinationPath))
                {
                    ResultService.Succeeded = false;
                    ResultService.Errors.Add("Directory not exits");
                }
            });
        }

        private Task<string> BuildFileNameAsync(string filename)
        {
            return Task.Run(() => 
            {
                var arrayName = filename.Split('.');
                arrayName[0] = string.Concat(arrayName[0], DateTime.Now.ToString());
                var nametmp = string.Join(".", arrayName);
                var finalName = nametmp.Replace("/", "_").Replace(":", "_").Replace(" ", "_");
                var finalTrim = finalName.Trim();
                return Path.Combine(_DestinationPath, finalTrim);
            });
        }

        private Task<string> GetFileNameAsync()
        {
            return Task.Run(() =>
            {
                return Path.GetFileName(_SourceFile);
            });
        }

        private Task CopyFileAsync(string sourceFile, string destFile)
        {

            return Task.Run(() =>
            {
                try
                {
                    File.Copy(sourceFile, destFile);
                }
                catch (Exception ex)
                {
                    ResultService.Succeeded = false;
                    ResultService.Errors.Add(ex.Message);
                }
                ResultService.Succeeded = true;
            });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioBackup.BusinessLogic
{
    public class ResultService
    {
        public ResultService()
        {
            Errors = new List<string>();
        }

        public bool Succeeded;
        public List<string> Errors;
    }
}

﻿using FactorioBackup.BusinessLogic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FactorioBackup.UI.Forms
{
    public partial class FactorioBackup : Form
    {
        public FactorioBackup()
        {
            InitializeComponent();
        }

        private async void BtnBackUp_Click(object sender, EventArgs e)
        {
            var serviceBackup = new BackupService(TxtSource.Text, TxtDestination.Text);
            var result = await serviceBackup.DoBackupAsync();

            if (result.Succeeded)
            {
                MessageBox.Show("Backup Completed");
            }
            else
            {
                result.Errors.ForEach(r => MessageBox.Show(r));
            }

        }

        private void BtnSourceSearch_Click(object sender, EventArgs e)
        {
            var fd = new OpenFileDialog();
            if (fd.ShowDialog() == DialogResult.OK)
            {
                TxtSource.Text = fd.FileName;
            }
        }

        private void BtnDestinationSearch_Click(object sender, EventArgs e)
        {
            var fd = new FolderBrowserDialog();
            if (fd.ShowDialog() == DialogResult.OK)
            {
                TxtDestination.Text = fd.SelectedPath;
            } 
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}

﻿namespace FactorioBackup.UI.Forms
{
    partial class FactorioBackup
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FactorioBackup));
            this.BtnBackUp = new System.Windows.Forms.Button();
            this.TxtSource = new System.Windows.Forms.TextBox();
            this.TxtDestination = new System.Windows.Forms.TextBox();
            this.LblSource = new System.Windows.Forms.Label();
            this.LblDestination = new System.Windows.Forms.Label();
            this.BtnSourceSearch = new System.Windows.Forms.Button();
            this.BtnDestinationSearch = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnBackUp
            // 
            this.BtnBackUp.Location = new System.Drawing.Point(498, 177);
            this.BtnBackUp.Name = "BtnBackUp";
            this.BtnBackUp.Size = new System.Drawing.Size(75, 23);
            this.BtnBackUp.TabIndex = 0;
            this.BtnBackUp.Text = "BackUp";
            this.BtnBackUp.UseVisualStyleBackColor = true;
            this.BtnBackUp.Click += new System.EventHandler(this.BtnBackUp_Click);
            // 
            // TxtSource
            // 
            this.TxtSource.Enabled = false;
            this.TxtSource.Location = new System.Drawing.Point(97, 93);
            this.TxtSource.Name = "TxtSource";
            this.TxtSource.Size = new System.Drawing.Size(452, 20);
            this.TxtSource.TabIndex = 1;
            // 
            // TxtDestination
            // 
            this.TxtDestination.Enabled = false;
            this.TxtDestination.Location = new System.Drawing.Point(97, 134);
            this.TxtDestination.Name = "TxtDestination";
            this.TxtDestination.Size = new System.Drawing.Size(452, 20);
            this.TxtDestination.TabIndex = 2;
            // 
            // LblSource
            // 
            this.LblSource.AutoSize = true;
            this.LblSource.Location = new System.Drawing.Point(45, 96);
            this.LblSource.Name = "LblSource";
            this.LblSource.Size = new System.Drawing.Size(44, 13);
            this.LblSource.TabIndex = 3;
            this.LblSource.Text = "Source:";
            // 
            // LblDestination
            // 
            this.LblDestination.AutoSize = true;
            this.LblDestination.Location = new System.Drawing.Point(26, 137);
            this.LblDestination.Name = "LblDestination";
            this.LblDestination.Size = new System.Drawing.Size(63, 13);
            this.LblDestination.TabIndex = 4;
            this.LblDestination.Text = "Destination:";
            // 
            // BtnSourceSearch
            // 
            this.BtnSourceSearch.Location = new System.Drawing.Point(555, 93);
            this.BtnSourceSearch.Name = "BtnSourceSearch";
            this.BtnSourceSearch.Size = new System.Drawing.Size(29, 20);
            this.BtnSourceSearch.TabIndex = 5;
            this.BtnSourceSearch.Text = "...";
            this.BtnSourceSearch.UseVisualStyleBackColor = true;
            this.BtnSourceSearch.Click += new System.EventHandler(this.BtnSourceSearch_Click);
            // 
            // BtnDestinationSearch
            // 
            this.BtnDestinationSearch.Location = new System.Drawing.Point(555, 134);
            this.BtnDestinationSearch.Name = "BtnDestinationSearch";
            this.BtnDestinationSearch.Size = new System.Drawing.Size(29, 20);
            this.BtnDestinationSearch.TabIndex = 6;
            this.BtnDestinationSearch.Text = "...";
            this.BtnDestinationSearch.UseVisualStyleBackColor = true;
            this.BtnDestinationSearch.Click += new System.EventHandler(this.BtnDestinationSearch_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(97, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(452, 73);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // FactorioBackup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 211);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.BtnDestinationSearch);
            this.Controls.Add(this.BtnSourceSearch);
            this.Controls.Add(this.LblDestination);
            this.Controls.Add(this.LblSource);
            this.Controls.Add(this.TxtDestination);
            this.Controls.Add(this.TxtSource);
            this.Controls.Add(this.BtnBackUp);
            this.Name = "FactorioBackup";
            this.Text = "Factorio Backup";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnBackUp;
        private System.Windows.Forms.TextBox TxtSource;
        private System.Windows.Forms.TextBox TxtDestination;
        private System.Windows.Forms.Label LblSource;
        private System.Windows.Forms.Label LblDestination;
        private System.Windows.Forms.Button BtnSourceSearch;
        private System.Windows.Forms.Button BtnDestinationSearch;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

